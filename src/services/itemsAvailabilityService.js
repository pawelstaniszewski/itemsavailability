angular.module('itemsAvailabilityApp')
  .factory('ItemsAvailabilityService', function() {

    var ranges = [];

    /**
     * Adds range to ranges collection
     * @param {object} range
     */
    function add(range) {
      ranges.push(range);
    };

    /**
     * Removes range from ranges collection
     * @param {object} range
     */
    function remove(range) {
      var idx = ranges.indexOf(range);

      ranges.splice(idx,1);
    };

    /**
     * Removes all ranges
     */
    function removeAll() {
      ranges.splice(0, ranges.length);
    }

    /**
     * Checks if given item has any date ranges defined
     * @param {number} item
     * @return {boolean}
     */
    function hasItemDateRanges(item) {
      var result = false;

      result = ranges.filter(function(range) {
        return (range.item == item);
      }).length > 0;

      return result;
    }

    /**
     * Sends data to service
     */
    function sendData() {
      removeAll();
    }

    return {
      ranges: ranges,
      add: add,
      remove: remove,
      sendData: sendData,
      hasItemDateRanges: hasItemDateRanges
    }
  });