angular.module('itemsAvailabilityApp')
  .factory('ItemsService', function() {

    var items = [1, 2, 3, 4, 5];

    /**
     * Returns items collection
     * @return {array}
     */
    function get() {
      return items;
    }

    return {
      get: get
    }
  });