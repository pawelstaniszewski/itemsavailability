angular.module('itemsAvailabilityApp')
  .constant('PARAMS', {
    'DATETIME_FORMAT_API': 'YYYY-MM-DDTHH:mmZ',
    'DATETIME_FORMAT_VIEW': 'yyyy-MM-dd HH:mm Z'
  })
  .constant('MESSAGES', {
    'SELECT_ITEM': 'Select an item',
    'ACCEPT_AND_SEND': 'Accept and send data',
    'NO_ITEM_SELECTED': 'No item selected',
    'UNAVAILABILITY_OF_ITEM': 'Unavailability of item',
    'NO_DATE_RANGES': 'No date ranges defined',
    'ADD_DATE_RANGES': 'Add date ranges during which an item is not available',
    'ADD': 'Add',
    'RESET': 'Reset',
    'FROM': 'From',
    'TO': 'To',
    'LIST_OF_DATE_RANGES': 'List of date ranges defined for item',
    'ERROR': 'Error',
    'DATE_FROM_REQUIRED': 'Date from is required',
    'DATE_TO_REQUIRED': 'Date to is required',
    'DATE_RANGE_INVALID': 'Date to can\'t be smaller then date from',
    'DATE_RANGE_OVERLAP': 'Date range overlaps previously added rangese'
  });