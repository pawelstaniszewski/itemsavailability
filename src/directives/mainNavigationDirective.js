angular.module('itemsAvailabilityApp')
  .directive('mainNavigation', function() {

    return {
      restrict: 'A',
      templateUrl: '/partials/mainNavigation.html'
    };
  });