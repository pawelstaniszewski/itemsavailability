angular.module('itemsAvailabilityApp')
  .directive('dateRangeValidation', function() {

    /** Validates if date to is not before date from
     * @param {string} from
     * @param {string} to
     * @param {string} format
     * @return {boolean}
     */
    isRangeValid = function(from, to, format) {
      var isValid = true,
          dateFrom,
          dateTo;

      if (from && to) {
        dateFrom = moment(from, format);
        dateTo = moment(to, format);

        if (dateFrom.isAfter(dateTo)) {
          isValid = false;
        }
      }

      return isValid;
    }

    /** Validates if date range overlaps any other range
     * @param {string} from
     * @param {string} to
     * @param {string} format
     * @param {array} otherRanges
     * @return {boolean}
     */
    isRangeUnique = function(from, to, format, otherRanges) {
      var dateFrom = moment(from, format),
          dateTo = moment(to, format),
          isValid = true,
          otherRangesLength = otherRanges.length,
          i = 0;

      if (from && to && angular.isArray(otherRanges)) {
        while(isValid && i < otherRangesLength) {
          isValid = !(
            dateFrom.isBefore(moment(otherRanges[i].to, format)) &&
            moment(otherRanges[i].from, format).isBefore(dateTo)
          );

          i++;
        }
      }

      return isValid;
    }

    return {
      // restrict to an atribute type
      restrict: 'A',
      // element must have ng-model attribute
      require: 'ngModel',
      link: function(scope, element, attrs, ctrl) {

        // set the validation to false until user changes the model
        ctrl.$setValidity('invalidDateRange', true);

        // set the validation to true
        ctrl.$setValidity('dateRangeOverlap', true);

        // watch if whole model has changed
        scope.$watch(attrs.ngModel, function(newVal) {
          if (angular.isDefined(newVal)) {
            var range = newVal,
                isValid = false;

            isValid = isRangeValid(range.from, range.to, attrs.datetimeFormat);

            ctrl.$setValidity('invalidDateRange', isValid);

            if (isValid) {
              isValid = isRangeUnique(range.from, range.to, attrs.datetimeFormat, scope.getCurrentItemDateRanges());

              ctrl.$setValidity('dateRangeOverlap', isValid);
            }
          }
        }, true);
      }
    };
  });