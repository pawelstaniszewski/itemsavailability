angular.module('itemsAvailabilityApp')
  .directive('itemAvailabilityList', function() {

    return {
      restrict: 'A',
      templateUrl: '/partials/itemAvailabilityList.html'
    };
  });