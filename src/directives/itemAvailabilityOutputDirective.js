angular.module('itemsAvailabilityApp')
  .directive('itemAvailabilityOutput', function() {

    return {
      restrict: 'A',
      templateUrl: '/partials/itemAvailabilityOutput.html'
    };
  });