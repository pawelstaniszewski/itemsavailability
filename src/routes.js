angular.module('itemsAvailabilityApp')
  .config(function($routeProvider) {

    $routeProvider
      .when('/api/availability/:item', {
        templateUrl: 'partials/itemAvailability.html',
        controller: 'ItemsAvailabilityController'
      })
      .otherwise({
        redirectTo:'/api/availability',
        templateUrl: 'partials/selectItem.html'
      });
  });