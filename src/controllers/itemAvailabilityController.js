angular.module('itemsAvailabilityApp')
  .controller('ItemsAvailabilityController', ['$scope', '$routeParams', 'ItemsAvailabilityService', 'PARAMS', 'MESSAGES', function($scope, $routeParams, ItemsAvailabilityService, PARAMS, MESSAGES) {

    var self = this;

    self.range = {from: '', to: ''};

    /**
     * Get ranges from ItemsAvailabilityService
     */
    $scope.ranges = ItemsAvailabilityService.ranges;

    /**
     * Submits dateRangeForm
     */
    function submit() {
      if ($scope.dateRangeForm.$valid) {
        // If form is valid, add range to ranges
        add();
      }
      else {
        // If form is invalid, show errors
        $scope.dateRangeForm.submitted = true;
      }
    }

    /**
     * Resets dateRangeForm
     */
    function reset() {
      self.range = {from: '', to: ''};
      $scope.dateRangeForm.submitted = false;
      $scope.dateRangeForm.$setPristine();
    }

    /**
     * Adds range to ranges collection and resets form
     */
    function add() {
      ItemsAvailabilityService.add({
        from: self.range.from,
        to: self.range.to,
        availability: "false",
        item: $scope.item
      });

      reset();
    };

    /**
     * Removes range from ranges collection
     */
    function remove(range) {
      ItemsAvailabilityService.remove(range);
    };

    /**
     * Checks dateRangeForm is submitted
     * @return {boolean}
     */
    function isSubmitted() {
      return ($scope.dateRangeForm.submitted === true);
    }

    /**
     * Checks if given item has any date ranges defined
     * @param {number} item
     * @return {boolean}
     */
    function hasItemDateRanges(item) {
      return ItemsAvailabilityService.hasItemDateRanges(item);
    }

    /**
     * Checks if current item has any date ranges defined
     * @return {boolean}
     */
    function hasCurrentItemDateRanges() {
      return hasItemDateRanges($scope.item);
    }

    /**
     * Returns array of date ranges defined for current item
     * @return {array}
     */
    function getCurrentItemDateRanges() {
      return $scope.ranges.filter(function(range) {
        return (range.item == $scope.item);
      })
    }

    angular.extend($scope, {
      submit: submit,
      remove: remove,
      reset: reset,
      isSubmitted: isSubmitted,
      hasCurrentItemDateRanges: hasCurrentItemDateRanges,
      getCurrentItemDateRanges: getCurrentItemDateRanges
    });

  }]);