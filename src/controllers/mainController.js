angular.module('itemsAvailabilityApp')
  .controller('MainController', ['$scope', '$routeParams', 'ItemsService', 'ItemsAvailabilityService', 'PARAMS', 'MESSAGES', function($scope, $routeParams, ItemsService, ItemsAvailabilityService, PARAMS, MESSAGES) {

    /**
     * Get items from ItemsService
     */
    $scope.items = ItemsService.get();

    /**
     * Returns info if any item is submitted
     * @return {boolean}
     */
    function isItemSelected() {
      return angular.isDefined($scope.item);
    }

    /**
     * Sends data to service
     */
    function sendItemsAvailabilityData() {
      ItemsAvailabilityService.sendData();
    }

    // Watch for item change
    $scope.$on('$routeChangeSuccess', function() {
      if ($routeParams.item) {
        $scope.item = $routeParams.item;
      }
    })

    angular.extend($scope, {
      isItemSelected: isItemSelected,
      sendItemsAvailabilityData: sendItemsAvailabilityData,
      PARAMS: PARAMS,
      MESSAGES: MESSAGES
    });

  }]);