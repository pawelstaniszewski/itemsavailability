# Task: Availability

## Description

Imagine you have a system for availability of a product for certain dates & times.

The system accepts inputs for new availability entries as list of ranges of timestamps.
Assume the following format:

    [
     {"from":"<ISO8601-RFC3339>",
      "to":"<ISO8601-RFC3339>",
      "availability":false,
      "item":<ID>}
    ]

So, for example:

    [
     {"from":"2016-07-01T00:00+02:00",
      "to":"2016-11-30T12:00+01:00",
      "availability":false,
      "item": 1}
    ]

So, we are only considering unavailability entries for now.

## Task

Please create a simple css-styleable page in angular that allows the user
to add date ranges during which an item is not available. You can assume
a fixed item as input.

It should live under the url /api/availability/<item>.
It should be possible to add multiple ranges at once.
It should be possible to remove wrongly chosen ranges.
